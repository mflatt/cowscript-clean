INCS = cmdline.h parse.h moo.h mode.h
SRCS = main.cpp cmdline.cpp parse.cpp moo.cpp

cowscript: $(SRCS) $(INCS)
	$(CXX) $(CXXFLAGS) -o cowscript $(SRCS)
