#include <iostream>
#include <stdexcept>
#include "cmdline.h"
#include "mode.h"
#include "parse.h"
#include "moo.h"

int main(int argc, char **argv) {
  try {
    int modes = use_arguments(argc, argv);
    while (has_more(std::cin)) {
      int n = parse(std::cin);
      if (modes & bell_mode)
        std::cerr << "<clank>\n";
      moo(n, modes, std::cout);
      if (modes & bell_mode)
        std::cerr << "</clank>\n";
    }
    return 0;
  } catch (std::runtime_error exn) {
    std::cerr << exn.what() << "\n";
    return 1;
  }
}
