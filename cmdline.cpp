#include <string>
#include <stdexcept>
#include "cmdline.h"
#include "mode.h"

static int add_mode(int modes, int new_mode, std::string flag);

int use_arguments(int argc, char **argv) {
  int modes = 0;

  for (int i = 1; i < argc; i++) {
    if (argv[i] == (std::string)"--bell") {
      modes = add_mode(modes, bell_mode, "--bell"); 
    } else if (argv[i] == (std::string)"--loud") {
      modes = add_mode(modes, loud_mode, "--loud"); 
    } else if (argv[i] == (std::string)"--french") {
      modes = add_mode(modes, french_mode, "--french"); 
    } else {
      throw std::runtime_error((std::string)"unrecognized argument: "
                               + argv[i]);
     }
  }

  return modes;
}

static int add_mode(int modes, int new_mode, std::string flag) {
  if (modes & new_mode)
    throw std::runtime_error("extra " + flag + " flag");
  return modes | new_mode;
}
