#include <iostream>
#include <stdexcept>
#include <cassert>
#include "parse.h"

static int parse_num(std::istream &in);
static void consume(std::istream &in, int expect);
static void skip_whitespace(std::istream &in);

int has_more(std::istream &in) {
  skip_whitespace(in);
  return !in.eof();
}

int parse(std::istream &in) {
  skip_whitespace(in);

  int c = in.peek();
  if (isdigit(c))
    return parse_num(in);

  throw std::runtime_error((std::string)"unexpected input: " + (char)c);
}

static int parse_num(std::istream &in) {
  int n = 0;

  while (1) {
    int c = in.peek();
    if (isdigit(c)) {
      consume(in, c);
      n = n*10 + (c - '0');
    } else
      break;
  }

  return n;
}

static void consume(std::istream &in, int expect) {
  int c = in.get();
  assert(c == expect);
}

static void skip_whitespace(std::istream &in) {
  while (1) {
    int c = in.peek();
    if (!isspace(c))
      break;
    consume(in, c);
  }
}
