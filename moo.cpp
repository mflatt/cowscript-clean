#include <iostream>
#include "mode.h"
#include "moo.h"

static void moo_once(int modes, std::ostream &out);

void moo(int n, int modes, std::ostream &out) {
  if (n > 0) {
    while (n > 1) {
      moo_once(modes, out);
      out << " ";
      n--;
    }
    moo_once(modes, out);
  }
  out << "\n";
}

static void moo_once(int modes, std::ostream &out) {
  if (modes & french_mode) {
    if (modes & loud_mode)
      out << "MEUH";
    else
      out << "meuh";
  } else {
    if (modes & loud_mode)
      out << "MOO";
    else
      out << "moo";
  }
}
